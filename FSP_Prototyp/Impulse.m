function impSound = Impulse(envSound, pulse,numChannel, electrodeRel)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% Einh�llende wird mit Elektrodenverh�ltnis multipliziert und           %
% impulsmoduliert.                                                      %
% Funktion erh�lt die Matrix mit den Einh�llenden und die mit dem       %
% Elektrodenverh�ltnis. Au�erdem wird die Matrix mit den Impulsen und   %
% die Anzahl der Kan�le ben�tigt.                                       %
% Gibt das impulsmodulierte Signal der oberen 15 Elektroden zur�ck.     %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

impSound=zeros(numChannel-8,numChannel-8,2); % Vorinitialisierung

for i=1:numChannel-8    % Modulation f�r jeden Kanal
    impSound(:,i,1) = electrodeRel(i+8,1) * envSound(i) * pulse(:,i);
    impSound(:,i,2) = electrodeRel(i+8,2) * envSound(i) * pulse(:,i);
end