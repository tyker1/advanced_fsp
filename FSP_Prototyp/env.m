function [envHigh,envLow,envLength,anzZero] = ...
    env(filtSound,zeroCrossing,numChannel, frameSize)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% Berechnung des arithmetischen Mittelwertes der Einh�llenden.          %
% Funtkion erh�lt die Matrix mit den gefilterten Daten und die          %
% Nulldurchg�nge. Au�erdem werden noch die Anzahl der Kan�le und die    %
% Segment gr��e ben�tigt.                                               %
% Gibt die Werte der Einh�llenden der oben 15 Kan�le in einer Matrix    %
% und die der unteren 8 Kan�le in einer weiteren Matrix zur�ck.         %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

envSound = filtSound(:,9:numChannel);   % Vorinitialisierung

for i = 9:numChannel                                % Berechnung der 
    envSound(:,i-8) = abs(hilbert(filtSound(:,i))); % Einh�llenden f�r die
end                                                 % oberen 15 Kan�le

envHigh=sum(envSound,1)/frameSize;  % Berechnung des Mittelwertes

persistent filtSoundPast
persistent pastZeroCrossing
if isempty(pastZeroCrossing)
    pastZeroCrossing = zeros(frameSize/2,8);
end
envLow = zeros(frameSize/2,8);

envLength = zeros(8,frameSize/2);
anzZero = zeros(8,1);
Envlope = zeros(frameSize,1);

for i = 1:8
    idx = find(zeroCrossing(:,i));
    Envlope=abs(hilbert(filtSound(:,i)));
    if isempty(idx) % Kein Nulldurchgang detektiert
  %  if (i ~= 0)
        envLow(:,i) = mean(abs(hilbert(filtSound(:,i))));
        envLength(i,:) = frameSize;
        anzZero(i) = 0;
        continue;
    end
    
    % Nulldurchgaenge detektiert
    anzZero(i) = length(idx); %
    StartIdx = 1;
    for j = 1:anzZero(i)
        EndIdx = zeroCrossing(idx(j),i);
        envLength(i,j) = EndIdx-StartIdx+1;
        envLow(j,i) = mean(Envlope(StartIdx:EndIdx));
        StartIdx = EndIdx+1;        
    end
    
    EndIdx = frameSize; %letzten Teil: Letzter Nulldurchgang zu End
    envLow(anzZero(i)+1,i) = mean(Envlope(StartIdx:EndIdx));
    envLength(i,anzZero(i)+1) = EndIdx-StartIdx+1;
end
