function zeroCrossing = findZeros(filtSound, frameSize)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% Detektierung der Nullstellen f�r die ersten acht Frequenzb�nder.      %
% Die Funktion erh�lt die gefilterten Daten. Au�erdem wird "frameSize"  %
% ben�tigt.                                                             %
% Gibt die Position der Nullstellen zur�ck.                             %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
persistent lastByte;

if isempty(lastByte)
    lastByte = zeros(1,8);
end
zeroCrossing = zeros(frameSize/2,8);    % Vorinitialisierung der
                                        % Ausgabematrix
for k=1:8
    j = 1;  % Zeile, in die die n�chste Nullstelle geschrieben werden soll
    for i=2:frameSize
        
       % if ((filtSound(i,k)>0) && (filtSound(i-1,k)<0)) % Nulldurchgang?
       if (((filtSound(i,k)*filtSound(i-1,k)) <= 0) && ((filtSound(i-1,k)+filtSound(i,k) ~= 0)))
         zeroCrossing(j,k) = i; % Index des Nulldurchgangs wird gespeichert
         j = j+1;   % N�chste Nullstelle wird in die n�chste Zeile
       end         % gespeichert
        
    end 
    
end