global frameSize;
global numElectrode;
global numChannel;
global filterOrd;
global filterOrdSyn;
global midF;
global numVirtual;
global Q;
global fs;
global coeffA
global coeffB;
global virtChannel;
global pulse;

Pfad = '../../advanced_pacev/AudioSample/';
DateiName = 'spfg.wav';
SimTime = 8; %in Sekunde
Fullpath = strcat(Pfad,DateiName);

%% Datei Einlesen und vorverarbeiten
[Origin, fs] = audioread(Fullpath);

%% Initialisierung
init_fsp;


%% Frameweiseverteilen und Verarbeiten
GesamtFrames = round(SimTime * fs / frameSize);
SimSamples = GesamtFrames * frameSize;
SimData = Origin(1:SimSamples);
synData = zeros(SimSamples,1);
for frameIdx = 1:GesamtFrames
%for frameIdx =120:130
    frameStart = (frameIdx-1)*frameSize + 1;
    frameEnd = frameIdx * frameSize;
    frame = SimData(frameStart:frameEnd);
    
    filtSound = filterbank(frame,numChannel,coeffB,coeffA,frameSize);
    zeroCrossing = findZeros(filtSound, frameSize);
    [envSound,envLow,envLength,anzZero] = env(filtSound,zeroCrossing,numChannel, frameSize);
    [peak, electrodeRel, channelIdx] = detect(filtSound, numChannel, fs, frameSize, virtChannel, numVirtual);
    impSound = Impulse(envSound, pulse,numChannel, electrodeRel);
    synSound  = syn(impSound,frameSize,numChannel,fs,midF,envLow,peak,...
                    envLength,anzZero,channelIdx, coeffA, coeffB, filterOrd, virtChannel);
    synData(frameStart:frameEnd) = synSound;
end
