function filtSound = filterbank( sound,numChannel,coeffB, coeffA, frameSize )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

filtSound = zeros(frameSize,numChannel);

for i = 1:numChannel
    filtSound(:,i) = filtfilt(coeffB(:,i),coeffA(:,i),sound);
end

end

