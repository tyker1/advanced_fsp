function [peak, electrodeRel, channelIdx] = ...
    detect(filtSound, numChannel, fs, frameSize, virtChannel, numVirtual)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% Berechnung des Frequenzmaximums f�r jeden Kanal und Bestimmung, in    %
% welchem virtuellen Kanal das Maximum liegt. Au�erdem wird das         %
% Verh�ltnis der Elektrodenpaare bestimmt.                              %
% Die Funktion erh�lt die Matrix mit dem gefilterten Signal. Au�erdem   %
% wird die Anzahl der Kan�le, Abtastfrequenz, Segmentgr��e, die Matrix  %
% mit der Aufteilung eines Frequenzbands in die virtuellen Kan�le und   %
% die Anzahl der virtuellen Kan�le ben�tigt.                            %
% Gibt eine Matrix mit den Mittelfrequenzen der virtuellen Kan�le in    %
% denen die Maxima liegen und das Elektrodenverh�ltnis f�r jeden Kanal  %
% zur�ck.                                                               %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = fs/2 * linspace(0,1,frameSize/2+1); % Frequenzachse
electrodeRel = zeros(numChannel,2); % Vorinitialisierung
peak = zeros(numChannel,1); % Vorinitialisierung
channelIdx = zeros(numChannel,1); %
for i = 1:numChannel                                        
   virtSpectrum = fft(filtSound(:,i)); % Berechnung des Spektrums 
   [maxi, idx] = max(abs(virtSpectrum(1:frameSize/2+1))); % Maximum suchen
    peak(i) = virtChannel(1,i);
   j=1; % erster virtueller Kanal
   
    while virtChannel(j,i)< f(idx)  % Wo liegt das Maximum?
        peak(i) = virtChannel(j,i); % Mittelfrequenz wird gespeichert
        j=j+1;  % n�chster virtueller Kanal
        
        if j >numVirtual-1    % Falls die Anzahl virtueller Kan�le 
         break              % �berschritten, ende
        end
        
    end
    if ((j ~= 1) && ((virtChannel(j,i)-f(idx)) > (f(idx)-virtChannel(j-1,i))))
        j = j-1;
    end
    peak(i) = virtChannel(j,i);
    channelIdx(i) = j;
    electrodeRel(i,:) = [1-((1/8)*(j-1)),(1/8)*(j-1)]; % Elektroden-
end                                                    % verh�ltnis