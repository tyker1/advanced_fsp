function synSound  = syn ...
    (impSound,frameSize,numChannel,fs,midF,envLow,peak,envLength,anzZero,channelIdx, coeffA, coeffB, filterOrd, virtChannel)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% Berechnung des synthetisierten Signals.                               %
% Erh�lt die Matrix mit den Impulsen, die mit den Werten der            %
% Einh�llenden der unteren acht Frequenzb�nder und die Positionen der   %
% Frequenzmaxima. Au�erdem wird die Segmentgr��e, Anzahl der Kan�le,    %
% Abtastfrequenz und die Mittelfrequenzen der Elektroden ben�tigt.      %
% Gibt das synthetisierte Signal zur�ck.                                %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

synSound=zeros(frameSize,1);    % Vorinitialisierung

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%       Sinusoidale-Synthese f�r die oberen 15 Kan�le                   %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

x0 = 0; % Beginn der Zeitachse
x1 = (frameSize-1)/fs;  % Ende der Zeitachse
t = linspace(x0,x1,frameSize)'; % Berechnung der Zeitachse
n = linspace(0,1,frameSize)';   % Vektor f�r die Amplitudeninterpolation

persistent lastamp % Matrix f�r die Amplitudeninterpolation
if isempty(lastamp)
    lastamp = zeros(15,8); % Vorinitialisierung
end

persistent phase % Vektor f�r die Speicherung der Phase
if isempty(phase)
    phase =zeros(15,8); % Vorinitialisierung
end

for i = 1:numChannel-8
%for i = 1:1
   IdxCh = channelIdx(i+8);
   for j = 1:8
       if j == IdxCh
           currentAmp = impSound(i,i,1)+impSound(i,i,2);  % aktuelle Amplitude
       else
           currentAmp = 0;
       end
       
       if ((lastamp(i,j) == 0) && (currentAmp == 0))
           continue;
       end
       
       z=sin(2*pi*t*virtChannel(j,i+8)+phase(i,j));  % Sinus erzeugen
       phi = 2*pi*virtChannel(j,i+8)*t; % Phasengang berechnen
       phase(i,j) = phi(end)+(2*pi*virtChannel(j,i+8)*1/fs)+phase(i,j); % neue Startphase       
       ampInt = lastamp(i,j)+(currentAmp-lastamp(i,j))*n; % Amplitudeninterpolation
       sinus = ampInt.*z;   % Sinus mit Amplitude multiplizieren
       synSound = synSound + sinus; % Sinus aufaddieren
       lastamp(i,j) = currentAmp; % Amplitude f�r n�chstes Segment speichern
   end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%           Sinusoidale-Synthese f�r die unteren 8 Kan�le               %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

persistent phaseLow % Phase f�r Sinus
if isempty(phaseLow)
    phaseLow = zeros(8,1);  % Vorinitialisierung
end

persistent ampLow % Matrix f�r Amplitudeninterpolation
if isempty(ampLow)
    ampLow = zeros(2,8);    % Vorinitialisierung
end

%% Neue Synthese
tl = phaseLow(1);
tr = tl + frameSize/fs;
t = linspace(tl,tr,frameSize);
phaseLow(1) = tr + 1/fs;
%n = zeros(1,frameSize);
for i = 1:8
    StartIdx = 1;
    sinus = sin(2*pi*midF(i).*t);
    ChannelSig = sinus;
    for j = 1:anzZero(i)+1
        SegLen = envLength(i,j);
        Ende = StartIdx+SegLen-1;
        n1 = 0:SegLen-1;
        ampLow(2,i) = envLow(j,i);
        InterAmp = ampLow(1,i) + (ampLow(2,i)-ampLow(1,i)).*n1/SegLen;
        ampLow(1,i) = envLow(j,i);
       % TempSin = ;
        ChannelSig(StartIdx:Ende) = sinus(StartIdx:Ende).*InterAmp;
        StartIdx = Ende + 1;
    end
    synSound = synSound + ChannelSig';
end

%%Alte Synthese

% envLength = round(envLength);
% 
% for i = 1:8
%     start_m = 1;
%     m = n(1:round(frameSize/envLength(i)):frameSize);
%     z = sin(2*pi*t*midF(i)+phaseLow(i));    % Sinus
%     sinus=z;
%     phi = 2*pi*midF(i)*t;   %Phasengang berechnen
%     phaseLow(i) = phi(end)+(2*pi*midF(i)*1/fs)+phaseLow(i);%neue Startphase
%     for j = 1:anzZero(i)
%           ende = start_m + length(m) - 1; 
%         if ende > frameSize
%             ende = frameSize;
%             p = linspace(0,1,ende-start_m+1)';
%             ampLow(2,i) = envLow(j,i); % Amplitude f�r erste H�lfte des Segments    
%             ampIntLow = ampLow(1,i)+(ampLow(2,i)-ampLow(1,i))*p; % Amplituden-
%                                                                  % interpolation
%         
%         elseif (ende < frameSize) && (j == anzZero(i))
%             ende = frameSize;
%             p = linspace(0,1,ende-start_m+1)';
%             ampLow(2,i) = envLow(j,i); % Amplitude f�r erste H�lfte des Segments    
%             ampIntLow = ampLow(1,i)+(ampLow(2,i)-ampLow(1,i))*p; % Amplituden-
%                                                                  % interpolation
%                                                                      
%         else
%         
%             ampLow(2,i) = envLow(j,i); % Amplitude f�r erste H�lfte des Segments    
%                                                              % interpolation
%             ampIntLow = ampLow(1,i)+(ampLow(2,i)-ampLow(1,i))*m; % Amplituden-
%                                                                      
%         end
%         sinus(start_m:ende) = ampIntLow .* z(start_m:ende);
%         start_m = ende + 1;
%         ampLow(1,i) = ampLow(2,i);  % Amplitude der ersten H�lfte f�r die 
%                                     % zweite H�lfte speichern
%     end
%     synSound = synSound + sinus;    % Sinus aufaddieren
% end

