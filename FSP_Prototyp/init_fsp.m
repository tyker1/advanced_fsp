%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%   Festlegung von Parametern und Berechnung von Variablen, die nur     %
%   einmal f�r die gesamte Simulation berechnet werden m�ssen           %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%                           Parameter                                   %
%                                                                       %   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global frameSize;
global numElectrode;
global numChannel;
global filterOrd;
global filterOrdSyn;
global midF;
global numVirtual;
global Q;
global fs;
global coeffA
global coeffB;
global virtChannel;
global pulse;

frameSize = 512;    % L�nge eines Segments
numElectrode = 24;  % Anzahl der Elektroden
filterOrd = 6;  % Filterordung d f�r die Filter der Filterbank
filterOrdSyn = 12;
midF = [50 150 250 350 450 570 700 840 ...
        1000 1170 1370 1600 1850 ...
        2150 2500 2900 3400 4000 4800 ...
        5800 7000 8500 10500 13500];  %Mittelfrequenzen der Elektroden
Q = 6;  % Qualit�t der Filter
numVirtual = 8; % Anzahl der virtuellen Kan�le zwischen zwei Elektroden

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%        Vorinitialisierung, dient der Beschleunigung des Codes         %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% fs = get_param('fsp/FromMultimediaFile','MaskDisplay');
% fs = regexp(fs,'[0-9]+ Hz','match');
% fs = strrep(fs,' Hz','');
% fs = str2double(fs);
%fs = 44100;
numChannel = numElectrode-1;    % Anzahl der Kan�le
virtChannel = zeros(numVirtual, numChannel);
phase=zeros(numChannel,1);  % Phase f�r die Synthese
coeffB = zeros(filterOrd+1,numChannel); % Filterkoeffizienten
coeffA = zeros(filterOrd+1,numChannel); % Filterkoeffizienten
coeffBSyn = zeros(filterOrdSyn+1,numChannel); % Filterkoeffizienten
coeffASyn = zeros(filterOrdSyn+1,numChannel); % Filterkoeffizienten
cutF1 = zeros(numChannel,1);
cutF2 = zeros(numChannel,1);
WnUnten = zeros(2,8);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%            Berechnung Filterkoeffizienten f�r die Filterbank          %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:8
    cutF1(i) = (midF(i) - midF(i) /(2*Q))/(0.5*fs);
    cutF2(i) = (midF(i) + midF(i) /(2*Q))/(0.5*fs);
    WnUnten(:,i) = [cutF1(i);cutF2(i)];
end

for i  = 9:numElectrode
    cutF1(i) = (midF(i) - midF(i) / (2 * Q)) / (0.5 * fs);   % Untere Grenzfrequenz
    cutF2(i) = (midF(i) + midF(i) / (2 * Q)) / (0.5 * fs);   % Obere Grenzfrequenz
end

WnOben =[cutF1(9:23), cutF2(10:24)]';
Wn = [WnUnten,WnOben];
i = 1;

while (i <= numChannel) && (Wn(1,i) < 1) && (Wn(2,i) < 1)
    [coeffB(:,i),coeffA(:,i)] = butter(filterOrd/2,Wn(:,i),'bandpass');
    i = i + 1;
end

i = 1;
while (i <= numChannel) && (Wn(1,i) < 1) && (Wn(2,i) < 1)
    [coeffBSyn(:,i),coeffASyn(:,i)] = butter(filterOrdSyn/2,Wn(:,i),'bandpass');
    i = i + 1;
end

numChannel = i - 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%                       Berechnung der Impulsmatirx                     %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pulse = zeros(numChannel-8);
j = 1;
for i=1:numChannel-8
    pulse(j,i) = ones(1); % Einheitsmatrix
    j = j+1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
%                   Berechnung der virtuelle Kan�le                     %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:numChannel
   diff=(midF(i+1)-midF(i))/(numVirtual-1);     % Aufteilung der 
   virtChannel(:,i) = (midF(i):diff:midF(i+1)); % Frequenzb�nder in
end                                             % virtuelle Kan�le